//
//  TFHelpers.swift
//

import Foundation
import UIKit

open class TFHelpers {
    
    // MARK: - Settings from PList files
    open class func getPListFile(_ objectTarget: AnyObject) -> NSDictionary{

        let bundle = Bundle(for: type(of: objectTarget))
        let path = bundle.path(forResource: "Info", ofType: "plist")
        return NSDictionary(contentsOfFile: path!)!
    }
        
    // MARK: - Misc
    open class func isAppExtension(_ objectTarget: AnyObject) -> Bool{
    
        let bundle = Bundle(for: type(of: objectTarget))
        return bundle.bundlePath.hasSuffix(".appex")
    }
    
    open class func getDocumentDirectory() -> String{
        
        let dirs = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        let dir = dirs[0]
        return dir
    }
    
    open class func validateEmail(_ email: String) -> Bool {
        
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    open class func getVersion(_ objectTarget: AnyObject) -> String {
        let bundle = Bundle(for: type(of: objectTarget))
        let appInfo = bundle.infoDictionary
        let shortVersion = appInfo!["CFBundleShortVersionString"] as! String
        let bundleVersion = appInfo!["CFBundleVersion"] as! String
        
        return "\(shortVersion) (\(bundleVersion))"
    }
    
    open class func getBatteryLevel() -> Float{
    
        //Battery Level: 1.000000 = 100% / 0.950000 = 95%
        UIDevice.current.isBatteryMonitoringEnabled = true
        let batteryLevel = UIDevice.current.batteryLevel
        return batteryLevel * 100
    }
}
