//
//  NSDateFormatterExtension.swift
//

import Foundation

public extension DateFormatter{
    
    convenience init(overrideLocale:Bool){
        self.init()
        if(overrideLocale){
            self.locale = Locale(identifier: "en_US")
        }
    }
}
