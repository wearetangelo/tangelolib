//
//  UIButtonExtension.swift
//

import Foundation
import UIKit

private var xoAssociationKey: UInt8 = 0
private var soAssociationKey: UInt8 = 0

public extension UIButton {
    
    var spinner: UIActivityIndicatorView? {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? UIActivityIndicatorView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var oldTitle: String? {
        get {
            return objc_getAssociatedObject(self, &soAssociationKey) as? String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &soAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func setBackgroundColor(_ color: UIColor, forState: UIControlState) {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()?.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    func startLoading(_ indicatorStyle: UIActivityIndicatorViewStyle = .white) {
        
        oldTitle = self.title(for: UIControlState())
        self.setTitle("", for: UIControlState())
        spinner = UIActivityIndicatorView(activityIndicatorStyle: indicatorStyle)
        spinner?.startAnimating()
        spinner?.frame = self.bounds
        self.addSubview(spinner!)
        self.isEnabled = false;
    }
    
    override func stopLoading() {
        
        spinner?.removeFromSuperview()
        self.setTitle(oldTitle, for: UIControlState())
        self.isEnabled = true;
    }
    
    func underlineTitleLabel(){
    
        let titleString : NSMutableAttributedString = NSMutableAttributedString(string: titleLabel!.text!)
        titleString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, (titleLabel!.text!).characters.count))
        setAttributedTitle(titleString, for: UIControlState())
        setAttributedTitle(titleString, for: UIControlState.highlighted)
    }
}
