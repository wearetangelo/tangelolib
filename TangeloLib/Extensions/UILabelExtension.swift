//
//  UILabelExtension.swift
//

import Foundation

public extension UILabel {

    func heightForLabel() -> CGFloat{

        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = self.lineBreakMode
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        return label.frame.height
    }

}
