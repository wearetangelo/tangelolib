//
//  UIViewExtension.swift
//

import Foundation
import UIKit

private var xoAssociationKey: UInt8 = 0
private var soAssociationKey: UInt8 = 0

public extension UIView {
    
    var loadingView: UIView? {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? UIView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    
    func loadFromNibNamed(_ nibNamed: String) -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibNamed, bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func changePosition(duration myDuration: TimeInterval, wait myWait: TimeInterval, offset myOffset: CGFloat, goUp myGoUp: Bool){
        
        let movement = (myGoUp ? -myOffset : myOffset)
        
        UIView.beginAnimations("anim", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDelay(myWait)
        UIView.setAnimationDuration(myDuration)
        
        self.frame = self.frame.offsetBy(dx: 0, dy: movement);
        
        UIView.commitAnimations()
    }    
    
    func rotateView(_ rotation: CGFloat, animated: Bool){
        
        if animated{
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = CGAffineTransform(rotationAngle: rotation)
            })
        }
        else{
            self.transform = CGAffineTransform(rotationAngle: rotation)
        }
    }
    
    func roundCorners(_ radius: CGFloat){
        
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func startLoading(_ viewColor: UIColor, indicatorStyle: UIActivityIndicatorViewStyle) {
        
        loadingView = UIView()
        loadingView?.backgroundColor = viewColor
        self.addSubview(loadingView!)
        self.bringSubview(toFront: loadingView!)
        loadingView?.centerViewToSuperView()
        loadingView?.fitViewToSuperView()
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: indicatorStyle)
        spinner.startAnimating()
        loadingView?.addSubview(spinner)
        spinner.centerViewToSuperView()
    }
    
    func stopLoading() {
        loadingView?.removeFromSuperview()
    }
    
    // MARK: - Layout Methods
    func fitViewToSuperView(){
        
        translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": self]
        
        superview!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        
        superview!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    func fitViewToBottom(){
        
        if let masterView = superview{
            
            translatesAutoresizingMaskIntoConstraints = false
            
            let views = ["view": self]
            
            masterView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        }
    }
    
    func fitViewWidthToSuperView(){
        
        translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": self]
        
        superview!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    func centerViewToSuperView(){
        
        translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["superview": self.superview!,"view": self]
        
        superview!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[superview]-(<=1)-[view]", options: NSLayoutFormatOptions.alignAllCenterX, metrics: nil, views: views))
        
        superview!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[superview]-(<=1)-[view]", options: NSLayoutFormatOptions.alignAllCenterY, metrics: nil, views: views))
    }
    
    func topSpaceToSuperview(_ mainView: UIView, space: CGFloat){
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": mainView]
        
        if let masterView = mainView.superview{
            
            masterView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(space)-[view]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        }
    }
    
    func leadingSpaceToSuperview(_ mainView: UIView, space: CGFloat){
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": mainView]
        
        if let masterView = mainView.superview{
            
            masterView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(space)-[view]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        }
    }
    
    func setWidthLayout(_ mainView: UIView, width: CGFloat){
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": mainView]
        
        mainView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view(\(width))]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    func setHeightLayout(_ mainView: UIView, height: CGFloat){
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": mainView]
        
        mainView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(\(height))]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
}
