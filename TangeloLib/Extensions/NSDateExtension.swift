//
//  DateExtension.swift
//

public extension Date{
    
    func toLocalTime() -> Date {
        let tz = TimeZone.autoupdatingCurrent
        let seconds = tz.secondsFromGMT(for: self)
        return Date(timeInterval: Double(seconds), since: self)
    }
    
    func toGlobalTime() -> Date {
        let tz = TimeZone.autoupdatingCurrent
        let seconds = -tz.secondsFromGMT(for: self)
        return Date(timeInterval: Double(seconds), since: self)
    }
    
    func dateAt(_ hours: Int, minutes: Int) -> Date{
        
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        
        var date_components = (calendar as NSCalendar).components([.year, .month, .day], from: self)
        
        date_components.hour = hours
        date_components.minute = minutes
        date_components.second = 0
        
        let newDate = calendar.date(from: date_components)!
                
        return newDate
    }
    
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool{
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending
        {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    
    func isLessThanDate(_ dateToCompare : Date) -> Bool{
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending
        {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func getHoursTimezone() -> String{
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ZZZZ"
        
        var stringDate = dateFormatter.string(from: self)
        
        stringDate = stringDate.replacingOccurrences(of: "GMT", with: "")
        
        return(stringDate)
    }
    
    func minutesFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
    }

    func secondsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
    }
    
    // MARK: - Datetime Formating
    func getISOStringForDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: self);
    }
    
    func getISOStringForDateTime() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        return dateFormatter.string(from: self);
    }
    
    func formatDateToDynamicProperties() -> String{
        
        var seconds = Date().timeIntervalSince(self)
        
        var textDif = ""
        
        if (seconds < 0) {
            seconds = 0
        }
        
        if (seconds <= 10){
            textDif = "Now";
        }
        else{
            if(seconds < 60){
                textDif = String(format:"%0.0f seconds ago", seconds)
            }else {
                if(seconds < 3600){
                    textDif = String(format:"%0.0f mins ago", seconds/60)
                }
                else {
                    if(seconds < 86400){
                        textDif = String(format:"%0.0f hours ago", seconds/3600)
                    }else {
                        if(seconds < 172800){
                            textDif = "Yesterday";
                        }else {
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "hh:mm a - MMM d";
                            textDif = dateFormatter.string(from: self)
                        }
                    }
                }
            }
        }
        
        return textDif;
    }
    
}
