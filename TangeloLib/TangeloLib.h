//
//  TangeloLib.h
//  TangeloLib
//
//  Created by Damian Dandrea on 5/18/16.
//  Copyright © 2016 Tangelo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TangeloLib.
FOUNDATION_EXPORT double TangeloLibVersionNumber;

//! Project version string for TangeloLib.
FOUNDATION_EXPORT const unsigned char TangeloLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TangeloLib/PublicHeader.h>


