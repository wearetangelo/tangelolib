//
//  TFNavigationDropDown.swift
//

import Foundation

public protocol TFNavigationDropDownDelegate {
    func onNavigationDropDownShow()
    func onNavigationDropDownHide()
}

open class TFNavigationDropDown: UIView {
    
    var arrowPadding: CGFloat = 15
    var animationDuration: TimeInterval = 0.3
    
    var maskBackgroundOpacity: CGFloat = 0.3
    
    var didSelectItemAtIndexHandler: ((_ indexPath: Int) -> ())?
    var isShown = false
    
    fileprivate weak var viewToShow: UIView!
    fileprivate var viewToShowHeightHandler: (() -> CGFloat)!
    fileprivate var viewToShowTopConstraint: NSLayoutConstraint!
    
    fileprivate weak var navigationController: UINavigationController!
    fileprivate var delegate: TFNavigationDropDownDelegate?
    fileprivate var menuButton: UIButton!
    fileprivate var menuTitle: UILabel!
    fileprivate var menuArrowImageView: UIImageView?
    fileprivate var menuWrapper: UIView!
    fileprivate var backgroundView: UIView!
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(navigationController: UINavigationController,
                delegate:TFNavigationDropDownDelegate?,
                align: NSTextAlignment,
                maskBackgroundColor: UIColor,
                maskBackgroundOpacity: CGFloat,
                title: String,
                titleFont: UIFont,
                titleColor: UIColor,
                bounds: CGRect,
                viewToShow: UIView,
                viewToShowHeightHandler: @escaping (() -> CGFloat),
                arrowImage: UIImage?){
        
        self.navigationController = navigationController
        self.delegate = delegate
        self.maskBackgroundOpacity = maskBackgroundOpacity
        self.viewToShow = viewToShow
        self.viewToShowHeightHandler = viewToShowHeightHandler
        
        // Set frame
        var width = (title as NSString).size(attributes: [NSFontAttributeName:titleFont]).width
        if let arrow = arrowImage{
            width = width + (arrowPadding + arrow.size.width)*2
        }
        let frame = CGRect(x: 0, y: 0, width: width, height: self.navigationController.navigationBar.frame.height)
        
        super.init(frame:frame)
        
        // Init button as navigation title
        self.menuButton = UIButton(frame: frame)
        self.menuButton.addTarget(self, action: #selector(TFNavigationDropDown.onTitleTap), for: UIControlEvents.touchUpInside)
        self.menuButton.backgroundColor = UIColor.clear
        self.addSubview(self.menuButton)
        
        self.menuTitle = UILabel(frame: frame)
        self.menuTitle.text = title
        self.menuTitle.textColor = titleColor
        self.menuTitle.font = titleFont
        self.menuTitle.textAlignment = align
        self.menuTitle.backgroundColor = UIColor.clear
        self.menuButton.addSubview(self.menuTitle)
        
        if let arrow = arrowImage{
            self.menuArrowImageView = UIImageView(image: arrow)
            self.menuButton.addSubview(self.menuArrowImageView!)
        }
        
        // Set up DropdownMenu
        
        self.menuWrapper =  UIView()//UIView(frame: CGRectMake(bounds.origin.x, 0, bounds.width, bounds.height))
        self.menuWrapper.isHidden = true
        
        // Add Menu View to container view
        self.navigationController.topViewController!.view.addSubview(self.menuWrapper)
        self.menuWrapper.fitViewToSuperView()
        
        //self.menuWrapper.clipsToBounds = true
        //self.menuWrapper.autoresizingMask = [ .FlexibleWidth, .FlexibleHeight ]
        
        // Init background view (under table view)
        self.backgroundView = UIView()//UIView(frame: bounds)
        self.backgroundView.backgroundColor = maskBackgroundColor
        //self.backgroundView.autoresizingMask = [ .FlexibleWidth, .FlexibleHeight ]
        
        let backgroundTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(TFNavigationDropDown.hideMenu))
        self.backgroundView.addGestureRecognizer(backgroundTapRecognizer)
        
        // Add background view & table view to container view
        self.menuWrapper.addSubview(self.backgroundView)
        self.menuWrapper.addSubview(self.viewToShow)
        
        self.backgroundView.fitViewToSuperView()
        
        //Layout ViewToShow
        self.viewToShow.clipsToBounds = true
        self.viewToShow.fitViewWidthToSuperView()
        
        viewToShowTopConstraint =  NSLayoutConstraint(item: self.viewToShow,
                                                      attribute: .top,
                                                      relatedBy: .equal,
                                                      toItem: self.menuWrapper,
                                                      attribute: .top,
                                                      multiplier: 1,
                                                      constant: 0)
        self.menuWrapper.addConstraint(viewToShowTopConstraint)
    }
    
    override open func layoutSubviews() {
        
        self.menuTitle.sizeToFit()
        
        if self.menuTitle.textAlignment == .left{
            self.menuTitle.frame.origin.y = (self.frame.size.height/2) - (self.menuTitle.frame.size.height/2)
        }
        else{
            self.menuTitle.center = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
        }
        
        if let menuArrow = menuArrowImageView{
            menuArrow.sizeToFit()
            menuArrow.center = CGPoint(x: menuTitle.frame.maxX + arrowPadding, y: self.frame.size.height/2)
        }
        self.menuWrapper.frame.origin.y = 0
    }
    
    func rotateArrow() {
        if let _ = menuArrowImageView{
            UIView.animate(withDuration: animationDuration, animations: {[weak self] () -> () in
                if let selfie = self {
                    selfie.menuArrowImageView!.transform = selfie.menuArrowImageView!.transform.rotated(by: 180 * CGFloat(M_PI/180))
                }
                })
        }
    }
    
    //MARK - User Iteraction
    func onTitleTap(){
        
        self.isShown == true ? hideMenu() : showMenu()
    }
    
    open func showMenu() {
        
        if self.isShown == false {
         
            self.menuWrapper.frame.origin.y = 0
            
            self.isShown = true
            
            delegate?.onNavigationDropDownShow()
            
            // Rotate arrow
            self.rotateArrow()
            
            // Visible menu view
            self.menuWrapper.isHidden = false
            
            // Change background alpha
            self.backgroundView.alpha = 0
            
            self.viewToShowTopConstraint.constant = -self.viewToShowHeightHandler()
            self.viewToShow.setNeedsUpdateConstraints()
            self.viewToShow.layoutIfNeeded()
            self.viewToShowTopConstraint.constant = 0
            self.viewToShow.setNeedsUpdateConstraints()
            
            self.menuWrapper.superview?.bringSubview(toFront: self.menuWrapper)
            
            UIView.animate(
                withDuration: animationDuration * 1.5,
                delay: 0,
                usingSpringWithDamping: 0.7,
                initialSpringVelocity: 0.5,
                options: [],
                animations: {
                    self.viewToShow.layoutIfNeeded()
                    self.backgroundView.alpha = self.maskBackgroundOpacity
                }, completion: nil
            )
        }
    }
    
    open func hideMenu() {
        
        if self.isShown == true {
            
            // Rotate arrow
            self.rotateArrow()
            
            self.isShown = false
            
            delegate?.onNavigationDropDownHide()
            
            // Change background alpha
            self.backgroundView.alpha = maskBackgroundOpacity
            
            self.viewToShowTopConstraint.constant = -self.viewToShowHeightHandler()
            self.viewToShow.setNeedsUpdateConstraints()
            
            UIView.animate(
                withDuration: animationDuration * 1.5,
                delay: 0,
                usingSpringWithDamping: 0.7,
                initialSpringVelocity: 0.5,
                options: [],
                animations: {
                    self.viewToShow.layoutIfNeeded()
                }, completion: nil
            )
            
            // Animation
            UIView.animate(
                withDuration: animationDuration,
                delay: 0,
                options: UIViewAnimationOptions(),
                animations: {
                    self.backgroundView.alpha = 0
                }, completion: { _ in
                    if self.isShown == false {
                        self.menuWrapper.isHidden = true
                    }
            })
        }
    }
    
    open func setMenuTitle(_ title: String) {
        self.menuTitle.text = title
        self.layoutSubviews()
    }
    
    open func applyBlurEffect(_ style: UIBlurEffectStyle){
        
        menuWrapper.backgroundColor = UIColor.clear
        menuWrapper.isOpaque = false
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.8
        blurEffectView.frame = menuWrapper.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        menuWrapper.insertSubview(blurEffectView, at: 0)
    }
    
    open func menuIsShowed() -> Bool{
        return self.isShown
    }
}
