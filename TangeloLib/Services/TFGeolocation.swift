//
//  TFGeolocation.swift
//

import Foundation
import CoreLocation

@objc public protocol TFGeolocationDelegate{
    @objc optional func onTFGeolocationChanged(locations: [CLLocation])
    @objc optional func onTFGeolocationReverseLocation()
}

open class TFGeolocation: NSObject, CLLocationManagerDelegate  {
    
    var delegate: TFGeolocationDelegate?
    
    let locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()

    open var latitude = "0"
    open var longitude = "0"
    open var coordinate: CLLocationCoordinate2D?
    open var placemark: CLPlacemark?
    
    public init(delegate: TFGeolocationDelegate?){
        
        super.init()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        
        self.delegate = delegate
    }
    
    open func startLocationWithAlwaysAuthorization(){
    
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }

    open func startLocationWithInUseAuthorization(){
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    open func stopLocationManager(){
    
        locationManager.stopUpdatingLocation()
    }
    
    func updateLocationInformation(_ locations: [CLLocation]){
    
        if let currentLocation = locations.last{
        
            coordinate = currentLocation.coordinate
            latitude = String(format: "%.8f", currentLocation.coordinate.latitude)
            longitude = String(format: "%.8f", currentLocation.coordinate.longitude)
            
            if let del = delegate{
                del.onTFGeolocationChanged?(locations: locations)
            }
            
            geoCoder.reverseGeocodeLocation(currentLocation, completionHandler: {(placemarks, error) -> Void in
                
                if error != nil {
                    print("GeoLocation Error: " + error!.localizedDescription)
                }
                else{
                    if let places = placemarks{

                        if places.count > 0 {
                            
                            if let place = places.last{

                                self.placemark = place
                                
                                if let del = self.delegate{
                                    del.onTFGeolocationReverseLocation?()
                                }
                            }
                        }
                    }
                }
            })
        }
    }
    
    open func getFullAddress(_ streetFirst: Bool = false) -> String{
    
        var fullAddress = ""
        var streetName = ""
        var streetNumber = ""
        
        if let place = placemark{
            
            if let thoroughfare = place.thoroughfare{
                
                streetName = thoroughfare.replacingOccurrences(of: "Calle ", with: "")
                streetName = streetName.replacingOccurrences(of: "Street ", with: "")
                
                if let subThoroughfare = place.subThoroughfare{
                    
                    streetNumber = subThoroughfare
                }
            }
        }
        
        if streetName != ""{
            if streetFirst{            
                fullAddress = streetName + " " + streetNumber
            }
            else{
                fullAddress = streetNumber + " " + streetName
            }
        }
        
        return fullAddress
    }
    
    //MARK: - CLLocationManagerDelegate
    open func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("GeoLocation Error: " + error.localizedDescription)
    }
    
    open func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        updateLocationInformation(locations)
    }    
}


