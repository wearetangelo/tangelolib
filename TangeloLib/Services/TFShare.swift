//
//  TFShare.swift
//

import Foundation

@objc public protocol TFShareDelegate{
    @objc optional func onTFShareDone()
    @objc optional func onTFShareCancel()
}

open class TFShare: NSObject, UIActivityItemSource {
    
    var viewController: UIViewController!
    var body: String = ""
    var emailSubject: String = ""
    var twitterUser: String?
    var phoneNumber: String?
    var delegate: TFShareDelegate?
    
    public init(viewController: UIViewController, body: String, emailSubject: String, twitterUser: String?, phoneNumber: String?, delegate: TFShareDelegate?){
        
        super.init()
        
        self.viewController = viewController
        self.body = body
        self.emailSubject = emailSubject
        self.twitterUser = twitterUser
        self.phoneNumber = phoneNumber
        self.delegate = delegate
    }
    
    open func share(){
        
        let activity = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        activity.completionWithItemsHandler = {(activityType: UIActivityType?, completed:Bool, returnedItems:[Any]?, error: Error?) in
            
            if let del = self.delegate{
                
                if (completed) {
                    del.onTFShareDone?()
                }
                else{
                    del.onTFShareCancel?()
                }
            }
        }
        self.viewController.present(activity, animated: true, completion: nil)
    }
    
    open func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return body;
    }
    
    open func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        
        var result = body
        
        if(activityType == UIActivityType.postToTwitter){
            
            if let user = twitterUser{
                
                result = body + " via " + user
            }
        }
        
        return result
    }
    
    open func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String {
        
        var result = body
        
        if (activityType == UIActivityType.mail){
            result = emailSubject
        }
        else{
            if (activityType == UIActivityType.message){
                
                if let phone = self.phoneNumber{
                    
                    result = phone
                }
            }
            else{
                if(activityType == UIActivityType.postToTwitter){
                    
                    if let user = twitterUser{
                        
                        result = body + " via " + user
                    }
                }
            }
        }
        
        return result
    }
}
