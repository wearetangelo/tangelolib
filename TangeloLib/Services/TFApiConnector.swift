//
//  TFApiConnector.swift
//

// Prefix methods with:
//      getXXX  > return one
//      listXXX > return one or more
//      createXXX
//      updateXXX
//      deleteXXX

import Foundation
import Alamofire

open class TFApiConnector {
  
    public init(){
    }
    
    // MARK: - MISC Methods
    open func parseResultOperation(_ response: DataResponse<Any>, callbackOk: (_ apiResult: AnyObject) -> Void, callbackError: (_ apiErrorResult: NSDictionary) -> Void){
        
        var statusCode = 500
        
        if let originalStatusCode = response.response?.statusCode{
            statusCode = originalStatusCode
        }
        
        if response.result.isSuccess || (statusCode<400){
            
            self.parseSuccessOperation(response,
                                       callbackOk: callbackOk,
                                       callbackError: callbackError)
        }
        else{
            self.parseErrorOperation(response,
                                     error: response.result.error,
                                     callbackError: callbackError)
        }
    }
    
    
    open func parseSuccessOperation(_ response: DataResponse<Any>, callbackOk: (_ apiResult: AnyObject) -> Void, callbackError: (_ apiErrorResult: NSDictionary) -> Void){
        
        var errorMessage = "The request could not be processed."
        var statusCode = 500
        
        if let originalStatusCode = response.response?.statusCode{
            statusCode = originalStatusCode
        }
        
        if statusCode >= 400{
        
            print("<--START ERROR-->")
            print(response.request)
            print(response.response)
            print("<--END ERROR-->")
        }
        
        do {
            let json: Any = try JSONSerialization.jsonObject(with: response.data!, options: [])
            
            if let dict = json as? NSDictionary {
                
                if statusCode >= 400{
                    
                    if let message = dict["message"] as? String{
                    
                        errorMessage = message
                    }
                    
                    let error = ["internal_error": statusCode, "message": errorMessage] as [String : Any]
                    
                    callbackError(error as NSDictionary)
                }
                else{
                    callbackOk(dict)
                }
            }
            else {
                if let array = json as? NSArray {
                    callbackOk(array)
                }
                else{
                    let error = ["internal_error": statusCode, "message": "Unknown result API method."] as [String : Any]
                    
                    callbackError(error as NSDictionary)
                }
            }
        } catch _ as NSError {
            if statusCode == 200{
                
                let ok = ["status": statusCode, "message": "OK"] as [String : Any]
                
                callbackOk(ok as AnyObject)
            }
            else{
                let error = ["internal_error": statusCode, "message": "Could not parse JSON."] as [String : Any]
                
                callbackError(error as NSDictionary)
            }
        }
    }
    
    open func parseErrorOperation(_ response: DataResponse<Any>, error: Error?, callbackError: (_ apiErrorResult: NSDictionary) -> Void){
        
        var errorMessage = "The request could not be processed."
        var statusCode = 500
        
        if let originalStatusCode = response.response?.statusCode{
            statusCode = originalStatusCode
        }
        
        print("<--START ERROR-->")
        print(response.request)
        print(response.response)
        print("<--END ERROR-->")
        
        if let respondeData = response.data{
            
            if let message = NSString(data: respondeData, encoding: String.Encoding.utf8.rawValue){
                
                errorMessage = message as String
            }
            else{
                if error != nil{
                    errorMessage = error!.localizedDescription
                }
            }
        }
        else{
            if error != nil{
                errorMessage = error!.localizedDescription
            }
        }
        
        let error = ["internal_error": statusCode, "message": errorMessage] as [String : Any]
        callbackError(error as NSDictionary)
    }
    
    // MARK: - REST Methods
    open func apiPost(_ url: String, requestHeader: Dictionary<String, String>?, parameters: Dictionary<String, AnyObject>, callbackOk: @escaping (_ apiResult: AnyObject) -> Void, callbackError: @escaping (_ apiErrorResult: NSDictionary) -> Void){
        
        Alamofire.request(url,
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers: requestHeader)
            .validate()
            .responseJSON(completionHandler:{ (response) in
                self.parseResultOperation(response,
                                          callbackOk: callbackOk,
                                          callbackError: callbackError)
            })
    }
    
    open func apiDelete(_ url: String, requestHeader: Dictionary<String, String>?, callbackOk: @escaping (_ apiResult: AnyObject) -> Void, callbackError: @escaping (_ apiErrorResult: NSDictionary) -> Void){
        
        Alamofire.request(url,
                          method: .delete,
                          parameters: [:],
                          encoding: JSONEncoding.default,
                          headers: requestHeader)
            .validate()
            .responseJSON(completionHandler:{ (response) in
                
                if response.result.isSuccess {
                    
                    let ok = ["internal": "204", "message": "OK"]
                    
                    callbackOk(ok as AnyObject)
                }
                else{                    
                    self.parseErrorOperation(response,
                                             error: response.result.error!,
                                             callbackError: callbackError)
                }
            })
    }
    
    open func apiPut(_ url: String, requestHeader: Dictionary<String, String>?, parameters: Dictionary<String, AnyObject>, callbackOk: @escaping (_ apiResult: AnyObject) -> Void, callbackError: @escaping (_ apiErrorResult: NSDictionary) -> Void){
        
        Alamofire.request(url,
                          method: .put,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers: requestHeader)
            .responseJSON(completionHandler:{ (response) in
                
                self.parseResultOperation(response,
                                          callbackOk: callbackOk,
                                          callbackError: callbackError)
            })
    }
    
    open func apiGet(_ url: String, requestHeader: Dictionary<String, String>?, callbackOk: @escaping (_ apiResult: AnyObject) -> Void, callbackError: @escaping (_ apiErrorResult: NSDictionary) -> Void){
        
        Alamofire.request(url,
                          method: .get,
                          encoding: JSONEncoding.default,
                          headers: requestHeader)
            .responseJSON(completionHandler:{ (response) in
                
                self.parseResultOperation(response,
                                          callbackOk: callbackOk,
                                          callbackError: callbackError)
            })
    }
    
    open func apiPostImage(_ url: String, requestHeader: Dictionary<String, String>?, imageToUpload: UIImage, callbackOk: @escaping (_ apiResult: AnyObject) -> Void, callbackError: @escaping (_ apiErrorResult: NSDictionary) -> Void){
        
        let imageData = UIImageJPEGRepresentation(imageToUpload, 1.0)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(imageData!, withName: "image", fileName: "image.jpg", mimeType: "image/*")
            },
            to: url,
            method: .post,
            headers: requestHeader,
            encodingCompletion: {encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    print(upload)
                    upload.uploadProgress { progress in // main queue by default
                            print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    upload.validate()
                    upload.responseJSON { response in
                        
                        self.parseResultOperation(response,
                                                  callbackOk: callbackOk,
                                                  callbackError: callbackError)
                    }
                case .failure(let encodingError):
                    
                    print(encodingError)
                    let error = ["internal_error": 500, "message": "Encoding error"] as [String : Any]
                    callbackError(error as NSDictionary)
                }
        })
    }
    
    open func apiPostWithBody(_ url: String, requestHeader: Dictionary<String, String>?, body: AnyObject, callbackOk: @escaping (_ apiResult: AnyObject) -> Void, callbackError: @escaping (_ apiErrorResult: NSDictionary) -> Void){
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let header = requestHeader{
            for (key, value) in header {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        Alamofire.request(request)
            .responseJSON(completionHandler:{ (response) in
                
                self.parseResultOperation(response,
                                          callbackOk: callbackOk,
                                          callbackError: callbackError)
            })
    }
}
