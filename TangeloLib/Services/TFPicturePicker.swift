//
//  TFPicturePicker.swift
//

import Foundation
import RSKImageCropper

// delegate to TFPicturePicker
public protocol TFPicturePickerDelegate{
    func onTFPicturePickerPicture(_ filePath:String)
    func onTFPictureFullPicture()
}

// this creates a default picture picker that shows 2 actions: from gallery / from camera
open class TFPicturePicker: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate, RSKImageCropViewControllerDelegate {

    var viewController:UIViewController!
    
    var delegate: TFPicturePickerDelegate?
    
    var showCropFeature = false
    var showFullPicture = false

    public init(title: String, message:String, viewController: UIViewController, delegate: TFPicturePickerDelegate?, showCropFeature: Bool = false, showFullPicture: Bool = false){
        
        super.init()
        
        self.viewController = viewController
        self.showCropFeature = showCropFeature
        self.showFullPicture = showFullPicture
        self.delegate = delegate
        
        //Create the AlertController
        let actionSheetController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)

        
        if self.showFullPicture {
            //Create and add first option action
            let choosePictureAction: UIAlertAction = UIAlertAction(title: "View Picture", style: .default) { action -> Void in
                // call delegate
                self.delegate?.onTFPictureFullPicture()
            }
            actionSheetController.addAction(choosePictureAction)
            
        }

        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
                self.selectPhotoFromCamera()
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
                self.selectPhotoFromGallery()
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        viewController.present(actionSheetController, animated: true, completion: nil)
    }
    
    func selectPhotoFromCamera() {
        // select the picture from camera
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.camera
        viewController.present(picker, animated: true, completion: {})
    }
    
    func selectPhotoFromGallery() {
        // select a picture from gallery
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        viewController.present(picker, animated: true, completion: {})
    }
    
    func savePicture(_ image: UIImage){
    
        // create tmp files
        let pictureName = UUID().uuidString + ".temp"
        let filePathToWrite = (TFHelpers.getDocumentDirectory() as NSString).appendingPathComponent(pictureName)
        let imageData: Data = UIImagePNGRepresentation(image.fixImageOrientation())!
        let fileManager = FileManager.default
        fileManager.createFile(atPath: filePathToWrite, contents: imageData, attributes: nil)
        
        // call delegate
        self.delegate?.onTFPicturePickerPicture(filePathToWrite)
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        
        if showCropFeature{
        
            let cropImage = RSKImageCropViewController(image: image)
            cropImage.delegate = self
            
            viewController.navigationController?.pushViewController(cropImage, animated: true)
        }
        else{
            savePicture(image)
        }
        
        picker.dismiss(animated: true, completion: {})        
    }

    //MARK: - RSKImageCropViewControllerDelegate
    open func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    open func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        viewController.navigationController?.popViewController(animated: true)
        
        savePicture(croppedImage)
    }    
}


