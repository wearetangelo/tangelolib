# TangeloLib

[![CI Status](http://img.shields.io/travis/Damian/TangeloLib.svg?style=flat)](https://travis-ci.org/Damian/TangeloLib)
[![Version](https://img.shields.io/cocoapods/v/TangeloLib.svg?style=flat)](http://cocoapods.org/pods/TangeloLib)
[![License](https://img.shields.io/cocoapods/l/TangeloLib.svg?style=flat)](http://cocoapods.org/pods/TangeloLib)
[![Platform](https://img.shields.io/cocoapods/p/TangeloLib.svg?style=flat)](http://cocoapods.org/pods/TangeloLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TangeloLib is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TangeloLib', :git => 'https://bitbucket.org/wearetangelo/tangelolib/'
```

## Author

Damian, damian@itangelo.com

## License

TangeloLib is available under the MIT license. See the LICENSE file for more info.
